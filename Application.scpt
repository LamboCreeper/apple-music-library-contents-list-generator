tell application "iTunes"
	set results to ""
	repeat with _track in every track
		set results to results & (name of _track) & " by " & (artist of _track) & "
"
		try
			set results_file to ((path to desktop folder) as text) & "Apple Music Library Contents.txt" as text
			set the open_results_file to open for access file results_file with write permission
			set eof of the open_results_file to 0
			write results to the open_results_file starting at eof
			close access the open_results_file
		end try
		"File created at location: " & results_file
	end repeat
end tell