# Apple Music Library Contents List Generator

Generates a text document listing all the songs you have saved via Apple Music and/or iTunes.

## Installation & Usage
1. Open Apple Script Editor (located in `Applications/Utilities/Script Editor.app`)
2. Copy and paste the contents of `Applciation.scpt` into the code area.
3. Press the Compile button or do `CMD+K`
4. Press the Run button or do `CMD+R`
5. Navigate to your desktop, you should now see a file called `Apple Music Library Contents.txt`.